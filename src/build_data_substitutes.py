import pyexcel_ods
import common

data = "../data/data.json"

tax_data = common.get_taxonomy_concepts_json(data)
all_concepts = tax_data["data"]["concepts"]

def get_concepts_substitutes(concepts):
    dictionary = {}
    for concept in concepts:
        preferred_label = concept["preferred_label"]
        substitutes = concept["substitutes"]
        dictionary[preferred_label] = substitutes
    return dictionary

def render_table(concepts, out_filename_ods):
    dst = [["sentence1", "sentence2", "label"]]
    concepts = get_concepts_substitutes(concepts)
    for item in concepts.keys():
        for substitutes in concepts[item]:
            substitutes["substitutability_percentage"] = common.map_label(substitutes["substitutability_percentage"]) / 100
            dst.append([item.lower(), substitutes["preferred_label"].lower(),
                        substitutes["substitutability_percentage"]])
    pyexcel_ods.save_data(out_filename_ods, {None: dst})

render_table(all_concepts, '../data/' + 'data' + '.ods')

print("build data done!")
