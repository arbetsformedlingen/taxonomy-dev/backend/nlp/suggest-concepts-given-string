from sentence_transformers import SentenceTransformer, InputExample, losses, SentencesDataset
from sentence_transformers.evaluation import TripletEvaluator
from torch.utils.data import DataLoader
import pandas as pd
import torch

def initialize_torch():
    if torch.cuda.is_available():
        print("Using cuda!")
    else:
        print("Using cpu")
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print("Number of devices: " + str(torch.cuda.device_count()))
    for i in range(torch.cuda.device_count()):
        print("  * " + torch.cuda.get_device_name(i))
    return device

initialize_torch()

data = pd.read_csv('../data/h_ads.csv', usecols=['anchor-title', 'positive-title', 'negative-title'])
print("data shape:", data.shape)

tr_data = data[0:100000]
data_train = []
for a, p, n in zip(tr_data['anchor-title'].values,
                   tr_data['positive-title'].values,
                   tr_data['negative-title'].values):
    data_train.append(InputExample(texts=[a, p, n]))

model = SentenceTransformer("KBLab/sentence-bert-swedish-cased")
train_loss = losses.TripletLoss(model=model)

train_batch_size = 32
train_dataset = SentencesDataset(data_train, model)
train_dataloader = DataLoader(
    train_dataset, shuffle=False, batch_size=train_batch_size)


eval_data = data[100001:]
data_eval = []
for a, p, n in zip(eval_data['anchor-title'].values,
                   eval_data['positive-title'].values,
                   eval_data['negative-title'].values):
    data_eval.append(InputExample(texts=[a, p, n]))

evaluator = TripletEvaluator.from_input_examples(
    data_eval, write_csv=False
)

epochs = 1
warmup_steps = 1000
score = 0
counter = 0
while score < 0.8:
    counter += 1
    model.fit(
        train_objectives=[(train_dataloader, train_loss)],
        epochs=epochs,
        evaluator=evaluator,
        evaluation_steps=1000,
        warmup_steps=warmup_steps,
        output_path="./sentence-bert-swedish-cased-recipes",
        show_progress_bar=True
    )
    recipe_model = SentenceTransformer('sentence-bert-swedish-cased-recipes')
    score = evaluator(recipe_model)
    print("evaluator score:", score)

print("train model done!")

