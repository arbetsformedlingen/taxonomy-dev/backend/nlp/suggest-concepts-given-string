from common import normalize, recipe_model, model
import concept_distance_evaluation.core as cde
import matplotlib.patches as mpatches
from adjustText import adjust_text
import matplotlib.pyplot as plt
from itertools import cycle
import numpy as np

def concept_embedder_from_model(model):
    return lambda concept: normalize(model.encode(concept["label"]))

def analyze_results_triplets(dataset_results):
    correct_triplets = []
    incorrect_triplets = []

    for x in dataset_results:
        if x["correctly-classified?"]:
            correct_triplets.append(x)
        else:
            incorrect_triplets.append(x)

    return correct_triplets, incorrect_triplets

def return_anchor_label(model):
    model_arr = [i["anchor"]["label"] for i in model]
    return model_arr

def scatter_plot(title, correct_eval, incorrect_eval, figure_name,
                 correct_number, incorrect_number):
    # np.random.seed(19680801)
    #N = 1046
    N1=correct_number
    N2=incorrect_number
    x1 = np.random.rand(N1)
    y1 = np.random.rand(N1)
    area1 = (np.random.rand(N1)) ** 9

    x2 = np.random.rand(N2)
    y2 = np.random.rand(N2)
    area2 = (np.random.rand(N2)) ** 9

    plt.figure(figsize=(40, 40))
    plt.title(title, fontweight='bold')
    cyan_patch = mpatches.Patch(color='cyan', label='Correct')
    magenta_patch = mpatches.Patch(color='magenta', label='Incorrect')
    plt.legend(handles=[cyan_patch, magenta_patch])
    plt.scatter(x1, y1, s=area1)
    plt.scatter(x2, y2, s=area2)

    cluster = [plt.annotate(txt, (x1[i], y1[i]), c=next(cycle('c')), alpha=0.9) for i, txt in enumerate(correct_eval)] \
              + [plt.annotate(txt, (x2[i], y2[i]), c=next(cycle('m')), alpha=0.9) for i, txt in enumerate(incorrect_eval)]

    adjust_text(cluster)
    fig = plt.gcf()
    return plt.show(), fig.savefig(figure_name)

def barchart(correct_model_array1, correct_model_array2, incorrect_model_array1, incorrect_model_array2, figure_name):
    barWidth = 0.25
    plt.subplots(figsize=(20, 20))

    correct_evaluation = [len(correct_model_array1), len(correct_model_array2)]
    incorrect_evaluation = [len(incorrect_model_array1), len(incorrect_model_array2)]

    br1 = np.arange(len(correct_evaluation))
    br2 = [x + barWidth for x in br1]

    plt.bar(br1, correct_evaluation, color='c', width=barWidth,
            edgecolor='cyan', label='Correct')
    plt.bar(br2, incorrect_evaluation, color='m', width=barWidth,
            edgecolor='magenta', label='Incorrect')

    plt.xlabel('Model', fontweight='bold', fontsize=15)
    plt.ylabel('Evaluation', fontweight='bold', fontsize=15)
    plt.xticks([r + barWidth / 2 for r in range(len(correct_evaluation))],
               ['model', 'recipe model'])

    plt.legend()
    fig = plt.gcf()
    return plt.show(), fig.savefig(figure_name)

# evaluation
dataset = cde.load_datasets(cde.standard_dataset_paths)

results_model = cde.evaluate_concept_embedder(dataset, concept_embedder_from_model(model))
summary_model = cde.summarize(results_model)
print("summery model:", summary_model)

results_recipe_model = cde.evaluate_concept_embedder(dataset, concept_embedder_from_model(recipe_model))
summary_recipe_model = cde.summarize(results_recipe_model)
print("summery recipe model:", summary_recipe_model)

results_analyze = cde.analyze_results(results_recipe_model)
results_analyze.print_correct_triplets()
results_analyze.print_incorrect_triplets()

# visualize evaluation
correct_model, incorrect_model = analyze_results_triplets(results_model)
correct_model_array = return_anchor_label(correct_model)
incorrect_model_array = return_anchor_label(incorrect_model)
print("correct_model_array:", len(correct_model_array))
print("incorrect_model_array", len(incorrect_model_array))

correct_recipe_model, incorrect_recipe_model = analyze_results_triplets(results_recipe_model)
correct_recipe_model_array = return_anchor_label(correct_recipe_model)
incorrect_recipe_model_array = return_anchor_label(incorrect_recipe_model)
print("correct_recipe_model_array:", len(correct_recipe_model_array))
print("incorrect_recipe_model_array", len(incorrect_recipe_model_array))

scatter_plot("Correct and Incorrect Evaluation for Model", correct_model_array,
             incorrect_model_array, "../svg/eval_model_scatterplot.svg", 771, 275)

scatter_plot("Correct and Incorrect Evaluation for Recipe Model", correct_recipe_model_array,
             incorrect_recipe_model_array, "../svg/eval_recipe_model_scatterplot.svg", 838, 208)

barchart(correct_model_array, correct_recipe_model_array, incorrect_model_array, incorrect_recipe_model_array,
         "../svg/eval_barchart.svg")

