from pathlib import Path
import random
import common
import json
import os

tax_file = "https://data.jobtechdev.se/taxonomy/version/latest/query/all-concepts/all-concepts.json"

tax = json.loads(common.get_taxonomy_concepts_url(tax_file))
all_concepts = tax["data"]["concepts"]

per_type = {}
for concept in all_concepts:
    t = concept["type"]
    if not (t in per_type):
        per_type[t] = []
    per_type[t].append(concept)

model_path = str(Path(__file__).parent.joinpath('sbert_vectors'))

os.makedirs(model_path, exist_ok=True)

for k in per_type:
    lookup = common.make_nearest_concepts(per_type[k])
    lookup.save(os.path.join(model_path, k))

with open(os.path.join(model_path, "types.json"), "w") as file:
    json.dump(list(per_type.keys()), file)

with open(os.path.join(model_path, "all_concepts.json"), "w") as file:
    json.dump(tax, file)

print("build model done!")
