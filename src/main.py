from werkzeug.middleware.proxy_fix import ProxyFix
from flask_restx import Api, Resource, reqparse
from flask import Flask, jsonify
from dotenv import load_dotenv
from flask_cors import CORS
from pathlib import Path
import common
import json
import os

load_dotenv()
debug_mode = os.environ.get("DEBUG")
port = os.environ.get("PORT")
host = os.environ.get("HOST")
n_max = 30

base_model_path = str(Path(__file__).parent.joinpath('sbert_vectors'))
flask_app = Flask(__name__)
CORS(flask_app)
flask_app.wsgi_app = ProxyFix(flask_app.wsgi_app)
flask_app.config['JSON_SORT_KEYS'] = False

api = Api(app=flask_app,
          version="2.0.0",
          title="Semantic Concept Search API",
          description="""Endpoints for suggest concept search with ML model.          
          Disclaimer: Please note that this service is still in early beta and the results may not be suitable for production yet. 
          The suggestions given could also contain bias since it is based on vast amounts of natural texts from the internet. 
          For more information about the model being used please see https://huggingface.co/KB/bert-base-swedish-cased""")

parser = reqparse.RequestParser()
parser.add_argument('array_of_words', type=str,  action='append', help='Array of synonym words, e.g., Odla tomater, '
                                                                       'Baka bullar, Odla bananer, etc.', required=True)
parser.add_argument('concept_type', type=str, help='Concept types from Taxonomy, e.g., occupation-name, skill, '
                                                   'language, etc. If its empty, it should provide suggestions for all types.',
                    required=False)
parser.add_argument('limit_number', type=int, help='Limit number of response (max number is 30).', required=True)

name_space = api.namespace('semantic-concept-search', description='')

def test_get_concatenated_label(term, concept_type):
    lookup = load_tax_per_type(base_model_path)[concept_type]
    connected_concepts = common.get_connected_concepts(
        lookup, [term], 5)
    concatenated_label = "".join([x["label"] for x in connected_concepts])
    return concatenated_label

def test_get_concatenated_label_anytype(term):
    concept_type = ""
    tax_per_type = load_tax_per_type(base_model_path)
    concept_map = load_concept_map(base_model_path)
    results = (process_request([term], concept_type, 30, tax_per_type, concept_map))
    concatenated_label = "".join([x["preferred_label"] for x in results[term]])
    return concatenated_label

def get_all_concepts(model_path):
    tax_file = Path(model_path).joinpath("all_concepts.json")
    tax = common.get_taxonomy_concepts_json(tax_file)
    all_concepts = tax["data"]["concepts"]
    return all_concepts

def load_concept_map(model_path):
    all_concepts = get_all_concepts(model_path)
    return {c["id"]: c for c in all_concepts}

def load_tax_per_type(model_path):
    with open(os.path.join(model_path, "types.json"), "r") as file:
        types = json.load(file)
    return {t: common.load_nearest_concepts(os.path.join(model_path, t)) for t in types}

def process_request(array, concept_type, limit_number, tax_per_type, concept_map):
    dic = {}
    for word in array:
        string = word.lower()
        if concept_type:
            connected_concepts = common.get_connected_concepts(
                tax_per_type[concept_type], [string], min(n_max, int(limit_number)))
        else:
            connected_concepts_all_types = []
            print("Hello")
            for t in tax_per_type.keys():
                print(t)
                per_connected_concepts = common.get_connected_concepts_all_types(
                    tax_per_type[t], [string])
                connected_concepts_all_types.extend(per_connected_concepts)
            connected_concepts_all_types = common.remove_duplicate_label(connected_concepts_all_types)
            connected_concepts_all_types.sort(key=lambda x: x.get('distance'))
            connected_concepts = connected_concepts_all_types[:min(n_max, int(limit_number))]

        suggested_concepts = [{**concept_map[suggestion["id"]], "matched_label": suggestion["label"],
                               "distance": suggestion["distance"]} for suggestion in connected_concepts]
        dic[word] = suggested_concepts
    return dic

@name_space.route('/')
class MainClass(Resource):
    tax_per_type = load_tax_per_type(base_model_path)
    concept_map = load_concept_map(base_model_path)

    @api.doc(parser=parser)
    def post(self):
        try:
            args = parser.parse_args()
            array = args['array_of_words']
            concept_type = args['concept_type']
            limit_number = args['limit_number']
            return jsonify(process_request(array, concept_type, limit_number, self.tax_per_type, self.concept_map))
        except KeyError as e:
            name_space.abort(500, e.__doc__, status="Could not get connected concepts", statusCode="500")
        except Exception as e:
            name_space.abort(400, e.__doc__, status="Could not get connected concepts", statusCode="400")

if __name__ == '__main__':
    flask_app.run(debug=debug_mode, host=host, port=port)
