from sentence_transformers import SentenceTransformer
from sklearn.neighbors import NearestNeighbors
from collections.abc import Iterable
from urllib.request import urlopen
from functools import lru_cache
from pathlib import Path
import numpy as np
import time
import json
import os
n_max = 30

print(__file__)
#model_path = Path(__file__).parent.joinpath('sentence-bert-swedish-cased-recipes')
#recipe_model = SentenceTransformer(str(model_path))

model = SentenceTransformer("KBLab/sentence-bert-swedish-cased")

def map_label(label):
    if label == 25:
        label = 75
    else:
        label = 25
    return label

def remove_duplicate_label(sam_list):
    seen_labels = set()
    def keep_unique(x):
        n = len(seen_labels)
        seen_labels.add(x["label"])
        return n < len(seen_labels)
    return [x for x in sam_list if keep_unique(x)]

def get_taxonomy_concepts_json(filename):
    with open(filename, encoding="utf8") as f:
        return json.load(f)

def get_connected_concepts_all_types(nearest_concepts, string_input):
    assert isinstance(string_input, list)
    assert len(string_input) == 0 or isinstance(string_input[0], str)
    indices, distances = nearest_neighbors(string_input, nearest_concepts.neighbors)
    connected_concepts = connected_concepts_dic_all_types(nearest_concepts, string_input, indices, distances)
    return connected_concepts

def connected_concepts_dic_all_types(nearest_concepts, string_input, indices, distances):
    result = []
    for i, item in enumerate(string_input):
        for (index, distance) in zip(indices[i, :], distances[i, :]):
            label = nearest_concepts.tax_labels[index]
            result.append({"id": nearest_concepts.concept_ids[label], "label": label, "distance": str(distance)})
    return result

def get_connected_concepts(nearest_concepts, string_input, num):
    assert isinstance(string_input, list)
    assert len(string_input) == 0 or isinstance(string_input[0], str)
    indices, distances = nearest_neighbors(string_input, nearest_concepts.neighbors)
    connected_concepts = connected_concepts_dic(nearest_concepts, string_input, indices, distances, num)
    return connected_concepts

def connected_concepts_dic(nearest_concepts, string_input, indices, distances, num):
    result = remove_duplicate_label(
        connected_concepts_dic_all_types(
            nearest_concepts, string_input, indices, distances))
    limit_dic = result[:num]
    return limit_dic

class NearestConcepts:
    def __init__(self, concept_ids, tax_labels, vectors):
        vectors = np.array(vectors)
        neighbors = NearestNeighbors(n_neighbors=min(n_max, len(vectors))).fit(vectors)

        self.vectors = vectors
        self.tax_labels = tax_labels
        self.neighbors = neighbors
        self.concept_ids = concept_ids

    def save(self, prefix):
        with open(prefix + "_data.json", "w") as file:
            json.dump({"concept_ids": self.concept_ids, "tax_labels": self.tax_labels}, file)

        with open(prefix + "_matrix.npy", "wb") as file:
            np.save(file, self.vectors)

def load_nearest_concepts(prefix):
    with open(prefix + "_matrix.npy", "rb") as file:
        vectors = np.load(file)
    with open(prefix + "_data.json", "r") as file:
        data = json.load(file)
    return NearestConcepts(data["concept_ids"], data["tax_labels"], vectors)

def make_nearest_concepts(concepts):
    tax_labels = get_lowercase_labels(concepts)
    vectors = encode_vectors(tax_labels)
    concept_ids = concept_ids_dic(tax_labels, concepts)
    return NearestConcepts(concept_ids, tax_labels, vectors)

def encode_vectors(labels_list):
    progress = ProgressReporter(len(labels_list))
    dst = []
    for label in labels_list:
        progress.end_of_iteration_message("Encoding...")
        dst.append(normalize(model.encode(label)))
    return dst

def nearest_neighbors(string_input, neighbors):
    assert isinstance(string_input, list)
    assert len(string_input) == 0 or isinstance(string_input[0], str)
    distances, indices = neighbors.kneighbors(encode_vectors(string_input))
    return indices, distances

def get_labels_with_hidden_labels(concept):
    return [label for labels in [concept["alternative_labels"], concept["hidden_labels"],
                                 [concept["preferred_label"]]] for label in labels]


def get_labels_without_hidden_labels(concept):
    return [label for labels in [concept["alternative_labels"], [concept["preferred_label"]]] for label in labels]

def get_type(concepts, tax_type):
    tax_list = [x for x in concepts if x["type"] in [tax_type]]
    return tax_list

def get_lowercase_labels(tax_list):
    tax_labels = [get_labels_without_hidden_labels(concept) for concept in tax_list]
    return [label.lower() for label in flatten(tax_labels)]

def concept_ids_dic(tax_labels, tax_list):
    dic_id = {}
    label_set = set(tax_labels)
    for concept in tax_list:
        for label in get_lowercase_labels([concept]):
            if label in label_set:
                dic_id[label] = concept["id"]
    return dic_id

def normalize(x):
    return x / np.linalg.norm(x)

@lru_cache(360)
def get_taxonomy_concepts_url(url_tax):
    with urlopen(url_tax) as response:
        body = response.read()
    return body

def flatten(xss):
    return [x for xs in xss for x in xs]

def flatten2(l):
    for el in l:
        if isinstance(el, Iterable) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el

def compute_time_estimate(progress, elapsed_time):
    base = {"elapsed_time": elapsed_time,
            "progress": progress}
    if progress <= 0:
        return base
    time_per_progress = elapsed_time / progress
    remaining_progress = 1.0 - progress
    remaining_time = remaining_progress * time_per_progress
    return {**base,
            "time_per_progress": time_per_progress,
            "total_time": time_per_progress,
            "remaining_progress": remaining_progress,
            "remaining_time": remaining_time}

time_segmentation_seconds_breakdown = ["weeks", 7, "days", 24, "hours", 60, "minutes", 60, "seconds"]

def segment_time(t, segmentation=time_segmentation_seconds_breakdown):
    seg = list(reversed(segmentation))
    parts = []
    x = t
    while True:
        n = len(seg)
        unit = seg[0]
        if n == 1:
            parts.append((x, unit))
            break;
        else:
            d = seg[1]
            seg = seg[2:]
            xi = int(round(x))
            y = xi % d
            x = xi // d
            if 0 < y or (len(parts) == 0 and x == 0):
                parts.append((y, unit))
        if x == 0:
            break
    return parts

def format_time_segmentation(time_parts):
    n = min(2, len(time_parts))
    return ", ".join(map(lambda p: "{:d} {:s}".format(p[0], p[1]), reversed(time_parts[-n:])))

def format_seconds(seconds):
    return format_time_segmentation(segment_time(seconds))

def format_time_estimate(est):
    s = "Progress: {:d} %".format(int(round(100 * est["progress"])))

    def timeinfo(k, lab):
        if k in est:
            return "\n{:s}: {:s}".format(lab, format_seconds(est[k]))
        return ""

    return s + timeinfo("total_time", "Total") + timeinfo("elapsed_time", "Elapsed") + timeinfo("remaining_time",
                                                                                                "Remaining")

class ProgressReporter:
    def __init__(self, total_iterations, completed_iterations=0, elapsed=0):
        self.start = time.time() - elapsed
        self.total_iterations = total_iterations
        self.completed_iterations = completed_iterations
        self.rate_limiter = rate_limiter(1)

    def set_start(self, start):
        self.start = start

    def end_of_iteration(self):
        self.completed_iterations += 1

    def time_estimate(self):
        return compute_time_estimate(self.completed_iterations / self.total_iterations, time.time() - self.start)

    def elapsed(self):
        return time.time() - self.start

    def progress_report(self):
        return "Completed {:d} of {:d} iterations\n".format(
            self.completed_iterations, self.total_iterations) + format_time_estimate(self.time_estimate())

    def end_of_iteration_message(self, msg):
        self.end_of_iteration()
        if self.rate_limiter():
            print(msg)
            print(self.progress_report() + "\n")

    def to_data(self):
        return {"elapsed": self.elapsed(),
                "total_iterations": self.total_iterations,
                "completed_iterations": self.completed_iterations}

def rate_limiter(period=1):
    last = None

    def f():
        nonlocal last
        t = time.time()
        if (last is None) or (period + last) < t:
            last = t
            return True
        return False

    return f
