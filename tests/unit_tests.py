import numpy as np
import unittest
import os, sys
sys.path.insert(0, os.path.abspath("../src"))
import common
import main

class MyTestCase(unittest.TestCase):
    def test_taxonomy(self):
        #self.assertTrue(isinstance(main.tax, dict))
        concepts = main.get_all_concepts(main.base_model_path)
        self.assertLess(0, len(concepts))

    def test_remove_duplicate_label(self):
        self.assertEqual([], common.remove_duplicate_label([]))
        self.assertEqual([{"label": "skill"}], common.remove_duplicate_label([{"label": "skill"}]))
        self.assertEqual([{"label": "skill"}],
                         common.remove_duplicate_label([{"label": "skill"}, {"label": "skill"}]))
        self.assertEqual([{"label": "skill"}, {"label": "occupation-name"}],
                         common.remove_duplicate_label([{"label": "skill"}, {"label": "skill"}, {"label": "occupation-name"}]))

    def test_process_request(self):
        term = "odla tomater"
        array = [term]
        concept_type = "occupation-name"
        limit_number = 5
        tax_per_type = main.load_tax_per_type(main.base_model_path)
        concept_map = main.load_concept_map(main.base_model_path)
        results = (main.process_request(array, concept_type, limit_number, tax_per_type, concept_map))
        concatenated_label = "".join([x["preferred_label"] for x in results[term]])
        self.assertTrue("grönsaksodlare" in concatenated_label.lower())
        self.assertEqual(limit_number, len(results[term]))

    def test_get_connected_concepts(self):
        term = "odla tomater"
        concept_type = "occupation-name"
        lookup = main.load_tax_per_type(main.base_model_path)[concept_type]
        connected_concepts = common.get_connected_concepts(
            lookup, [term], 30)
        (a, b) = common.encode_vectors(["odla tomater", "grönsaksodlare"])
        self.assertLess(np.linalg.norm(a - b), 0.8)
        concatenated_label = "".join([x["label"] for x in connected_concepts])
        self.assertTrue("grönsaksodlare" in concatenated_label.lower())

    def test_process_request_anytype(self):
        term = "odla tomater"
        array = [term]
        concept_type = ""
        limit_number = 30
        tax_per_type = main.load_tax_per_type(main.base_model_path)
        concept_map = main.load_concept_map(main.base_model_path)
        results = (main.process_request(array, concept_type, limit_number, tax_per_type, concept_map))
        concatenated_label = "".join([x["preferred_label"] for x in results[term]])
        self.assertTrue("grönsaksodlare" in concatenated_label.lower())
        self.assertTrue("odla växter" in concatenated_label.lower())
        self.assertEqual(limit_number, len(results[term]))

    def test_process_request_anytype_array(self):
        array = ["Odla tomater", "Baka bullar", "Odla bananer", "terrorist"]
        for i in array:
            string = i.lower()
            if string == "odla tomater":
                concatenated_label = main.test_get_concatenated_label_anytype(string)
                self.assertTrue("grönsaksodlare" in concatenated_label.lower())
            if string == "baka bullar":
                concatenated_label = main.test_get_concatenated_label_anytype(string)
                self.assertTrue("bagare" in concatenated_label.lower())
            if string == "odla bananer":
                concatenated_label = main.test_get_concatenated_label_anytype(string)
                self.assertTrue("fruktodling" in concatenated_label.lower())
            if string == "terrorist":
               concatenated_label = main.test_get_concatenated_label_anytype(string)
               self.assertTrue("imam" in concatenated_label.lower())
               self.assertTrue("islam" in concatenated_label.lower())
               self.assertFalse("soldat" in concatenated_label.lower())
               self.assertTrue("syrien" in concatenated_label.lower())

    def test_get_connected_concepts_array(self):
        array = ["Odla tomater", "Baka bullar", "Odla bananer", "terrorist"]
        for i in array:
            string = i.lower()
            if string == "odla tomater":
                concatenated_label = main.test_get_concatenated_label(string, "occupation-name")
                self.assertTrue("grönsaksodlare" in concatenated_label.lower())
                (a, b) = common.encode_vectors(["odla tomater", "grönsaksodlare"])
                self.assertLess(np.linalg.norm(a-b), 0.8)
            if string == "baka bullar":
                concatenated_label = main.test_get_concatenated_label(string, "occupation-name")
                self.assertTrue("bagare" in concatenated_label.lower())
                (a, b) = common.encode_vectors(["baka bullar", "bagare"])
                self.assertLess(np.linalg.norm(a-b), 0.83)
            if string == "odla bananer":
                concatenated_label = main.test_get_concatenated_label(string, "skill")
                self.assertTrue("fruktodling" in concatenated_label.lower())
                (a, b) = common.encode_vectors(["odla bananer", "fruktodling"])
                self.assertLess(np.linalg.norm(a-b), 0.83)
            if string == "terrorist":
               concatenated_label = main.test_get_concatenated_label(string, "occupation-name")
               self.assertTrue("imam" in concatenated_label.lower())
               self.assertFalse("soldat" in concatenated_label.lower())
               self.assertFalse("syrien" in concatenated_label.lower())
               self.assertFalse("islam" in concatenated_label.lower())

if __name__ == '__main__':
    unittest.main()

