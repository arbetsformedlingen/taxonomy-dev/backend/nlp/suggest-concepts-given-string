# Semantic Concept Search

This API contains semantic concept search using ML model, Sentence-BERT model from KBLab – Kungliga biblioteket.

## Set up environment

### Running API:

- Make sure python 3.11 is installed.
- Make sure pip is installed.
- Set up a venv (IntelliJ or similar environment):
  - File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment
- Install dependencies:
  `pip install -r requirements.txt`

### Running evaluate sbert:

If you want to run evaluate_sbert.py you should install concept-distance-evaluation:

```sh
pip install git+https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/concept-distance-evaluation.git@master#egg=concept_distance_evaluation
```

### Running API in docker container:

```sh
docker image build -t flask_docker .
```

```sh
docker run -p 5000:5000 -d flask_docker
```

### Running API without docker container:

First run `build_model.py` to get sbert_vectors and then run `main.py`.
