# start by pulling the python image
FROM docker.io/library/python:3.13.2-slim-bookworm

# switch working directory
WORKDIR /app

# copy neccessary content from the local file to the image
COPY .env requirements.txt ./
COPY src ./src/

# install the dependencies and packages in the requirements file
RUN apt-get -y update &&\
    pip install --upgrade pip && pip install -r requirements.txt

# add sbert_vectors repository
ADD src/sbert_vectors ./sbert_vectors

# add port
EXPOSE 8080

# configure the container to run in an executed manner
ENTRYPOINT [ "python" ]

# specifies the default program that will execute once the container runs
CMD ["src/main.py"]
